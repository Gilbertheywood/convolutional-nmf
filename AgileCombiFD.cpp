#include "AgileCombiFD.h"
#define Max_N 999

using namespace std;
using namespace arma;

AgileCombiFD::AgileCombiFD(AgileCombiFDParameters _param) {
    param = _param;
}

mat AgileCombiFD::InsertRows(mat W, int rowcount, double val, bool top = true) {
    rowvec row0(W.n_cols);
    row0.fill(val);
    for (int r = 0; r < rowcount; r++) {
        if (top) {
            W.insert_rows(0, row0);
        } else {
            W.insert_rows(W.n_rows, row0);
        }
    }
    return W;
}

mat AgileCombiFD::Rec(mat W, vector<mat> H, vector<int> Phi) {
    int L = W.n_rows;
//    int K = W.n_cols;
    int N = H[0].n_cols;
    int M = H.size();
    assert((int)Phi.size() == (int)M);

    mat Rec = zeros<mat >(L, N);

    //int hi = 0;
    for (int i = 0; i < M; i++) {
        int p = Phi[i];
        mat WW = W.rows(0,L-p-1);
        WW = InsertRows(WW, p, 0.0);

        Rec += WW * H[i];
    }
    //cout << Rec << endl;
    //exit(1);
    return Rec;
}

double AgileCombiFD::KLdiv(mat D, mat Rec, bool show = false) {
    int L = D.n_rows;
    int N = D.n_cols;

    /*cout << "Dlog: " << endl;
    cout << D << endl;
    cout << endl;

    cout << "Rec: " << endl;
    cout << Rec << endl;
    cout << endl;*/

    double ckl = 0.0; // KL divergence(cost)
    for (int i = 0; i < L; i++) {
        for (int j = 0; j < N; j++) {
            double Dij = D(i,j);
            /*if (Dij < eps) {
                D(i,j) = 0;
                Dij = 0;
            }*/
            double Rij = Rec(i,j);
            /*if (Rij < eps) {
                Rec(i,j) = 0;
                Rij = 0;
            }*/
            if (show) printf("(%.10lf, %.10lf, %.10lf)\n", Dij, Rij, log((Dij+eps)/(Rij+eps)));
            if (show) printf("(%.10lf, %.10lf, %.10lf)\n",  (Dij+eps)/(Rij+eps), log((Dij+eps)/(Rij+eps)), Dij*log((Dij+eps)/(Rij+eps)));

            /*double logpart = Dij*log((Dij+eps)/(Rij+eps));
            if (Rij > eps) ckl += logpart - Dij+Rij;
            else if (Dij < eps) ckl += -Dij + Rij;
            else ckl += Dij + Rij;*/

            ckl += Dij*log((Dij+eps)/(Rij+eps)) - Dij + Rij;
            //ckl += abs(Rij-Dij);

            if (show) printf("%d: %.10lf\n", i, ckl);
        }
    }

    /*cout << D << endl;
    cout << Rec << endl;
    cout << endl;*/

    //exit(1);
    return ckl;
}

mat AgileCombiFD::KLupdate(mat D, mat &W, vector<mat> &H, vector<int> Phi, mat &VRec) {
    int L = W.n_rows;
    int K = W.n_cols;
    int N = H[0].n_cols;
    int M = H.size();

    double cost_old = KLdiv(D, VRec);

    vector<mat> H_old;
    for (int i = 0; i < M; i++) {
        mat Hi = H[i];
        H_old.push_back(Hi);
    }
    mat W_old = W;

    mat VR = D / (VRec+eps);
    mat O = ones<mat>(L, N);

    vector<mat> H_x;
    vector<mat> H_y;
    vector<mat> GradH;

    //printf("*\n");

    for (int i = 0; i < M; i++) {
        int p = Phi[i];
        mat Hi_x = W.rows(0, L-p-1).t()*VR.rows(p, L-1);
        H_x.push_back(Hi_x);
        mat Hi_y = W.rows(0, L-p-1).t()*O.rows(p, L-1);
        H_y.push_back(Hi_y);

        mat Gradi = Hi_x / (Hi_y);
        GradH.push_back(Gradi);
    }

    //printf("**\n");

    double nuH = 1.0;
    double accel = 1.0;
    while (true) {
        int pindex = 0;
        for (int i = 0; i < M; i++) {
//            int p = Phi[i];
            H[pindex] = H_old[pindex] % (pow(GradH[pindex], nuH));
            pindex++;
        }
        VRec = Rec(W, H, Phi);
        double cost = KLdiv(D, VRec);

        if (cost - cost_old > 0.0001 * cost) {
            nuH = std::max(nuH/2.0, 1.0);
        } else {
            nuH *= accel;
            cost_old = cost;
            break;
        }
    }

    //printf("***\n");


    // update W
    VR = D / (VRec + eps);
    mat W_x = zeros<mat>(L, K);
    mat W_y = zeros<mat>(L, K);

    //printf("N: %d, M: %d, K: %d, L: %d\n", N, M, K, L);
    //printf("VR: (%d, %d)\n", VR.n_rows, VR.n_cols);
    //printf("H[i]: (%d, %d)\n", H[0].n_rows, H[0].n_cols);

    for (int i = 0; i < M; i++) {
        int p = Phi[i];
        mat Wxp = VR.rows(p, L-1) * H[i].t();
        Wxp = InsertRows(Wxp, p, 0.0, false);
        W_x = W_x+Wxp;

        mat Wyp = O.rows(p, L-1) * H[i].t();
        Wyp = InsertRows(Wyp, p, 0.0, false);
        W_y = W_y+Wyp;
    }

    mat GradW = W_x / (W_y+eps);
    while (true) {
        W = W_old % (pow(GradW, nuH));
        W = NormalizeW(W);

        VRec = Rec(W, H, Phi);
        double cost = KLdiv(D, VRec);

        if (cost - cost_old > 0.0001 * cost) {
            nuH = max(nuH/2.0, 1.0);
        } else {
            nuH *= accel;
            cost_old = cost;
            break;
        }
    }
    /*
    cout << "W: " << endl;
    cout << W << endl;
    cout << "H[0]" << endl;
    cout << H[0] << endl;
    */
    return VRec;
}

void AgileCombiFD::InitW(mat &W) {
    int L = W.n_rows;
    int K = W.n_cols;

    //assert(param.initW.n_cols == K && param.initW.n_rows == L);

    //W = param.initW;

    for (int j = 0; j < K; j++) {
        for (int i = 0; i < L; i++)
            W(i,j) = (rand() % (Max_N+1))/float(Max_N+1);
            //W(i,j) = rand() % 121;
            //W(i, j) = ((i+1)*(j+1))/double(K*L);
    }

}

void AgileCombiFD::InitH(vector<mat> &H) {
    int K = H[0].n_rows;
    int N = H[0].n_cols;
    int M = H.size();

    for (int m = 0; m < M; m++) {
        //H[m] = param.initH/M;

        for (int k = 0; k < K; k++) {
            for (int n = 0; n < N; n++) {
                H[m](k, n) = (rand() % (Max_N+1))/float(Max_N+1);
                //H[m](k, n) = rand() % 121;
                //H[m](k, n) = ((k+1)*(n+1))/(double)(K*N);
            }
        }

    }
}

mat AgileCombiFD::NormalizeW(mat W) {
    double W2norm = norm(W, "fro");
    W /= W2norm+eps;
    return W;
}

void AgileCombiFD::NormalizeWH(mat &W, vector<mat> &H) {
    int L = W.n_rows;
    int K = H[0].n_rows;
    int N = H[0].n_cols;
    int M = H.size();

    for (int k = 0; k < K; k++) {
        double maxsumH = 0.0;
        for (int n = 0; n < N; n++) {
            double sumH = 0.0;
            for (int m = 0; m < M; m++) {
                sumH += H[m](k, n);
            }
            maxsumH = max(maxsumH, sumH);
        }
        for (int m = 0; m < M; m++) {
            for (int i = 0; i < N; i++) {
                double valH = H[m](k,i);
                H[m](k,i) = valH / maxsumH;
            }
        }
        for (int i = 0; i < L; i++) {
            double valW = W(i,k);
            W(i,k) = valW * maxsumH;
        }
    }
}

void AgileCombiFD::Solve(Instance data, double convergenceRate, int time_limit, char* sol_file) {
//    srand(time(0));
    time_t init_time, now_time;
    init_time = time(NULL);

    //freopen("output.txt", "w", stdout);

    int N = data.N;
    int M = param.M;
    int K = param.K;
    int L = data.L;

    mat D = data.XrdMat;

    double Qlogmin = log(data.qvalues[0]);
    double Qlogmax = log(data.qvalues[L-1]);
    double Qstepsize = (Qlogmax-Qlogmin)/(L-1);

    vector<double> Q;
    vector<double> Qlog;
    vector<double> Qlogsteps;

    for (int i = 0; i < L; i++) {
        Q.push_back(data.qvalues[i]);
        Qlog.push_back(Qlogmin+i*Qstepsize);
        Qlogsteps.push_back(exp(Qlogmin+i*Qstepsize));
    }

    //for (int i = 0; i < L; i++) printf("%.10lf\n", Qlogsteps[i]);
    //exit(1);

    mat Dlog;
    Dlog.copy_size(D);

    //printf("1\n");

    for (int i = 0; i < N; i++) {
        vector<double> xrd;
        xrd.clear();
        for (int j = 0; j < L; j++) {
            xrd.push_back(D(j, i));
        }
        //printf("*\n");
        vector<double> X(Q.begin(), Q.end());
        vector<double> Y(xrd.begin(), xrd.end());
        Spline s(Q, xrd);

        for (int j = 0; j < L; j++) {
            Dlog(j, i) = s(Qlogsteps[j]);
            if (Dlog(j, i) < 0.0) Dlog(j, i) = 0.0;
            //printf("%lf, %lf, %lf, %lf\n", D(j, i), Dlog(j, i), Qlogsteps[j], Q[j]);
        }
        //printf("**\n");
    }
    //exit(1);

    //cout << D << endl;

    //printf("2\n");

    // Init matrix W
    mat W = zeros<mat>(L,K);
    InitW(W);

    vector<mat> H;
    for (int m = 0; m < M; m++) {
        mat Hm = zeros<mat>(K,N);
        H.push_back(Hm);
    }
    InitH(H);

    W = NormalizeW(W);

    //printf("3\n");

    //Init Phi
    vector<int> Phi;
    for (int i = 0; i < M; i++) {
        Phi.push_back(i);
    }

    /*
    printf("W\n");
    cout << W << endl;
    printf("H[0]   %d\n", H.size());
    cout << H[0] << endl;
    */

    //cout << "D:" << endl;
    //cout << D << endl;

    mat DRec = Rec(W, H, Phi);
    double cost = KLdiv(Dlog, DRec);

    printf("cost: %lf\n", cost);

    //exit(1);
    now_time = time(NULL);
    int time_cost = now_time - init_time;
    printf("Before KLupdate, we have spent %d seconds.\n", time_cost);

    int iter_cnt = 0;
    while (true) {
        DRec = KLupdate(Dlog, W, H, Phi, DRec);
        //cout << Dlog << endl;
        //cout << DRec << endl;
        double new_cost = KLdiv(Dlog, DRec);
        printf("%d: old: %lf, new: %lf", iter_cnt++, cost, new_cost);
        now_time = time(NULL);
        time_cost = now_time - init_time;
        //if (abs(cost-new_cost) < cost*0.002+eps) break;
        //if (abs(cost-new_cost) < 0.0002*cost+eps) break;
        if ((cost-new_cost) < cost*convergenceRate+eps || time_cost > time_limit) break;
        else {
            cost = new_cost;
            if (iter_cnt % 50 == 0) printf("   Total time cost until now: %d s.", time_cost);
            printf("\n");
        }
        //exit(1);
    }
    NormalizeWH(W, H);
    //printf("4\n");

    vector<Phase> phases;
    vector<vector<PhaseMixture> > phase_mixtures;

    for (int k = 0; k < K; k++) {
        vector<double> xrd;
        for (int j = 0; j < L; j++) {
            xrd.push_back(W(j, k));
        }
        vector<double> X(Qlogsteps.begin(), Qlogsteps.end());
        vector<double> Y(xrd.begin(), xrd.end());

        Spline s(X, Y);

        Phase phase;
        for (int j = 0; j < L; j++) {
            phase.xrd.push_back(s(Q[j]));
        }

        phases.push_back(phase);

        vector<PhaseMixture> mixtures;
        for (int i = 0; i < N; i++) {
            double sumH = 0.0;
            double shiftH = 0.0;

            for (int m = 0; m < M; m++) {
                sumH += H[m](k,i);
                double shift = Qlogsteps[m]/Qlogsteps[0];
                shiftH += shift * H[m](k,i);
            }
            shiftH /= sumH;

            PhaseMixture mixture;
            mixture.isInvolved = (sumH > 0.0001);
            mixture.proportion = (sumH > 0.0001)?sumH:0.0;
            mixture.shift = shiftH;
            mixtures.push_back(mixture);
        }
        phase_mixtures.push_back(mixtures);
    }

    /*for (int k = 0; k < K; k++) {
        for (int i = 0; i < L; i++) {
            printf("%.3lf ", phases[k].xrd[i]);
        }
        printf("\n");
    }
    for (int k = 0; k < K; k++) {
        for (int i = 0; i < N; i++) {
            printf("%.3lf ", phase_mixtures[k][i].proportion);
        }
        printf("\n");
    }*/

    // generate sol txt
    generateSolTxt(phases, phase_mixtures, N, M, K, L, Q, Qlogsteps, sol_file);
}

void AgileCombiFD::generateSolTxt(vector<Phase> phases, vector<vector<PhaseMixture> > phase_mixtures, int N, int M, int K, int L, vector<double> Q, vector<double> Qlogsteps, char* sol_file) {
    freopen(sol_file, "w", stdout);
    printf("// Number of phases\n");
    printf("K=%d\n", K);

    printf("\n// Phase pattern (basis)\n");
    printf("Q=");
    for (int i = 0; i < L; i++) {
        double q = Q[i];
        printf("%.3lf", q);
        if (i == L-1) printf("\n");
        else printf(",");
    }
    for (int k = 0; k < K; k++) {
        printf("B%d=", k+1);
        for (int i = 0; i < L; i++) {
            printf("%.6lf", phases[k].xrd[i]);
            if (i == L-1) printf("\n");
            else printf(",");
        }
    }

    printf("\n// Phase concentrations at each sample\n");
    for (int  n = 0; n < N; n++) {
        double sum = 0.0;
        for (int k = 0; k < K; k++) {
            sum += phase_mixtures[k][n].proportion;
        }
        printf("C%d=", n+1);
        for (int k = 0; k < K; k++) {
            double c;
            if (sum > 0.0) c = phase_mixtures[k][n].proportion/sum;
            else c = 0.0;
            printf("%.6lf", c);
            if (k == K-1) printf("\n");
            else printf(",");
        }
    }

    printf("\n// Per-phase model for each sample\n");
    double* rlist = new double[L];
    for (int n = 0; n < N; n++) {
        for (int k = 0; k < K; k++) {
            printf("R%d_%d=", n+1, k+1);
            double shift = phase_mixtures[k][n].shift;
            double proportion = phase_mixtures[k][n].proportion;
            int l = 0;
            for (int i = 0; i < L; i++) rlist[i] = 0;
            for (int i = 0; i < L; i++) {
                double pos = Qlogsteps[i]*shift;
                if (pos < Qlogsteps[l]) {
                    if (abs(pos-Qlogsteps[l])<=0.5) rlist[l] += proportion * phases[k].xrd[i];
                } else {
                    while (abs(pos-Qlogsteps[l]) > 0.5 && l < L) l++;
                    if (abs(pos-Qlogsteps[l]) <= 0.5) {
                        rlist[l] += proportion * phases[k].xrd[i];
                    }
                }
            }

            for (int i = 0; i < L; i++) {
//                printf("%.6lf", phases[k].xrd[i]*phase_mixtures[k][n].proportion);
                printf("%.6lf", rlist[i]);
                if (i == L-1) printf("\n");
                else printf(",");
            }
        }
    }
    delete [] rlist;

    /*
    printf("\n// Phase shifts at each sample\n");
    for (int n = 0; n < N; n++) {
        printf("S%d=", n+1);
        for (int k = 0; k < K; k++) {
            printf("%.6lf", phase_mixtures[k][n].shift);
            if (k == K-1) printf("\n");
            else printf(",");
        }
    }*/
}
