#include <cstdio>
#include <time.h>
#include <iostream>
#include <cmath>
#include <cstring>
#include <vector>
#include <algorithm>
#include <string>
#include <armadillo>
#include "assert.h"
#include "Spline.h"
#include "Phase.h"
#include "PhaseMixture.h"
#include "Instance.h"

using namespace std;
using namespace arma;

class AgileCombiFDParameters {
public:
    int M, K;
    mat initW, initH;

    AgileCombiFDParameters(int _M, int _K) {
        M = _M;
        K = _K;
    }
    AgileCombiFDParameters() {
        M = 10;
        K = 8;
    }
};

class AgileCombiFD {
private:
    const static double eps = 0.0000000001;
    AgileCombiFDParameters param;

public:
    AgileCombiFD(AgileCombiFDParameters _param);
    mat InsertRows(mat, int, double, bool);
    mat Rec(mat, vector< mat >, vector<int>);
    double KLdiv(mat, mat, bool);
    mat KLupdate(mat, mat &, vector<mat > &, vector<int>,mat &);

    void InitW(mat &);
    void InitH(vector<mat> &);
    mat NormalizeW(mat W);

    void NormalizeWH(mat &W, vector<mat > &H);
    void Solve(Instance, double, int, char*);

    void generateSolTxt(vector<Phase> phases, vector<vector<PhaseMixture> > phase_mixtures, int N, int M, int K, int L, vector<double> Q, vector<double> Qlogsteps, char* sol_file);
};
