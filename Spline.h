#include <cstdio>
#include <iostream>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace std;

class Spline {
public:
    vector<double> X;
    vector<double> Y;

    Spline(vector<double> _X, vector<double> _Y) {
        X = _X;
        Y = _Y;
    }

    double operator () (double x) {
        int len = X.size();
        int lx = 0, rx = len-1;

        if (x >= X[rx]) return Y[rx];
        if (x <= X[lx]) return Y[lx];

        while (lx+1 < rx) {
            int mid =(lx+rx)/2;
            if (x == X[mid]) return Y[mid];
            else if (x < X[mid]) {
                rx = mid;
            } else {
                lx = mid;
            }
        }

        double y = Y[lx]+(x-X[lx])*(Y[rx]-Y[lx])/(X[rx]-X[lx]);
        return y;
    }
};
