#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <armadillo>
#include <vector>

using namespace std;
using namespace arma;

class Instance {
public:
    int N, L;
    mat XrdMat;
    vector<double> qvalues;
};
