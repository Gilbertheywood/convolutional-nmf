from __future__ import division
import readCsv
import pandas
from collections import defaultdict



'''
file to count the number of phases
'''


class phaseCounter:


	def __init__(self, art=False):

		self.tasks = set(list(readCsv.cleanSubmissionData(art=art)['taskId']))
		self.submissions = readCsv.cleanSubmissionData(art=art)
		self.taskTable = readCsv.getTaskData()


	def countPhases(self):
		"""
		gives a dictionary of the average number of phases used in different tasks
		"""

		phaseDict = defaultdict(list)
		for task in self.tasks:
			submissions = list(self.submissions[self.submissions['taskId'] == task]['targetrowlinestatus'])
			for submission in submissions:
				# note that submission is a string, we remove 4 to remove brackets, comma and 0
				phaseDict[task].append(len(set(submission))-4)

		averageDict = {}

		for key in phaseDict:
			averageDict[key] = sum(phaseDict[key])/len(phaseDict[key])


		samplepointDict = self.task2Samplepoint()
		outDict = defaultdict(list)

		# note that for some samples we have two tasks along different slices, average these 
		for key in averageDict:
			outDict[samplepointDict[key]].append(averageDict[key])

		return outDict


	def task2Samplepoint(self):
		"""
		gives the tasknumber to samplepoint
		"""
		taskDict = {}
		for task in self.tasks:
			taskDict[task] = int(self.taskTable[self.taskTable['id']==task]['imgPeakSelected'])
		return taskDict




#myclass = phaseCounter(art=True)
#mydict =  myclass.countPhases()
#print mydict
