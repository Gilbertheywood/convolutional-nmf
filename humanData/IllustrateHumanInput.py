from __future__ import division
from sklearn import gaussian_process
import numpy as np

from countPhases import phaseCounter

import matplotlib.pyplot as plt



'''
TODO
add noise and redo prediction where we are
tune parameters GP
'''


myclass = phaseCounter()
mydict =  myclass.countPhases()

with open('Ta-Rh-Pd_inst.txt') as f:
    content = f.readlines()

A = map(float, content[16][2:].split(','))
B = map(float, content[17][2:].split(','))
C = map(float, content[18][2:].split(','))


X = np.array([[A[i-1], B[i-1], C[i-1]] for i in mydict]) #note one-off error for indexing
#Y = np.array([sum(mydict[key])/len(mydict[key]) for key in mydict]) # average possibly different nmbrs from different tasks
Y = np.array([np.std(mydict[key]) for key in mydict]) # average possibly different nmbrs from different tasks
gp = gaussian_process.GaussianProcess(theta0=0.001, nugget=0.1)
gp.fit(X, Y)


# Now predict for all sample lcocations
Xpred = np.array([[A[i], B[i], C[i]] for i in range(len(A))])
Xindex = [i+1 for i in range(len(A))]
y_pred, sigma2_pred = gp.predict(Xpred, eval_MSE=True)
y_pred = map(str, list(y_pred))
Xindex = map(str, Xindex)


"""
XindexHave = [key for key in mydict]
y_predHave = [sum(mydict[key])/len(mydict[key]) for key in mydict]


XindexAll = []
y_predAll = []

# sieve both the lists together
for i in range(len(Xindex)+len(XindexHave)):
	try:
		if XindexHave[0] > Xindex[0]:
			XindexAll.append(Xindex.pop(0))
			y_predAll.append(y_pred.pop(0))
		else:
			XindexAll.append(XindexHave.pop(0))
			y_predAll.append(y_predHave.pop(0))

	# perhaps one list is empty
	except IndexError:
		if not Xindex:
			XindexAll.append(XindexHave.pop(0))
			y_predAll.append(y_predHave.pop(0))
		if not XindexHave:
			XindexAll.append(Xindex.pop(0))
			y_predAll.append(y_pred.pop(0))


"""

with open('interpolatedPhasestheta0.1Nugget0.01.txt', 'w') as f:
	f.write(','.join(list(y_pred))+'\n')
	f.write('\n')
	f.write(','.join(Xindex))


vec3 = np.array([0,0])
vec2 = np.array([1/2, np.sqrt(3/4)])
vec1 = np.array([1,0])


for i in range(len(Xindex)):
	Xindex[i] = A[i]*vec1 + B[i]*vec2 + C[i]*vec3

x = [array[0] for array in Xindex]
y = [array[1] for array in Xindex]

cm = plt.cm.get_cmap('RdYlBu')
sc = plt.scatter(x, y, c=y_pred
	, cmap=cm)
plt.colorbar(sc)
plt.show()







