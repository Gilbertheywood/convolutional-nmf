from sklearn import gaussian_process
import numpy as np

from countPhases import phaseCounter

'''
TODO
tune parameters GP
make sure the order in A,B,C is 1,2,3...

slices seems to go 0-44
'''

myclass = phaseCounter(art=True)
mydict =  myclass.countPhases()


with open('ART_inst.txt') as f:
    content = f.readlines()

A =  map(float, content[11][3:-2].split(','))
B =  map(float,content[12][3:-2].split(','))
C = map(float,content[13][3:-2].split(','))


X = np.array([[A[i], B[i], C[i]] for i in mydict]) #note no one-off error
Y = np.array([mydict[key][0] for key in mydict]) #note that the defaultdict contains lists of len 1
Xpred = np.array([[A[i], B[i], C[i]] for i in range(len(A)) if i not in mydict])
Xindex = map(str, [i for i in range(len(A)) if i not in mydict])
XindexHave = map(str, [i for i in mydict])
Yhave = map(str, [mydict[key][0] for key in mydict])

gp = gaussian_process.GaussianProcess(theta0=1e-2)
gp.fit(X, Y)
y_pred, sigma2_pred = gp.predict(Xpred, eval_MSE=True)

y_pred = list(y_pred)
print y_pred, Xindex, XindexHave, Yhave



with open('interpolatedPhasesART.txt', 'w') as f:
	f.write(','.join(Yhave)+'\n')
	f.write('\n')
	f.write(','.join(XindexHave))






