from __future__ import division
from sklearn import gaussian_process
import numpy as np

from countPhases import phaseCounter

import matplotlib.pyplot as plt

'''
TODO
tune parameters GP
make sure the order in A,B,C is 1,2,3...
'''


myclass = phaseCounter()
mydict =  myclass.countPhases()

with open('Ta-Rh-Pd_inst.txt') as f:
    content = f.readlines()

A = map(float, content[16][2:].split(','))
B = map(float, content[17][2:].split(','))
C = map(float, content[18][2:].split(','))

X = np.array([[A[i-1], B[i-1], C[i-1]] for i in mydict]) #note one-off error
Y = np.array([mydict[key][0] for key in mydict]) #note that the defaultdict contains lists of len 1
Xpred = np.array([[A[i], B[i], C[i]] for i in range(len(A)) if i+1 not in mydict])
Xindex = map(str, [i+1 for i in range(len(A)) if i+1 not in mydict])

gp = gaussian_process.GaussianProcess(theta0=0.01
	)
gp.fit(X, Y)
y_pred, sigma2_pred = gp.predict(Xpred, eval_MSE=True)
y_pred = map(str, list(y_pred))
print y_pred

print np.mean(gp.y_std)




Xindex = map(int, Xindex)
XindexHave = [key for key in mydict]
y_predHave = [sum(mydict[key])/len(mydict[key]) for key in mydict]


XindexAll = []
y_predAll = []

# sieve both the lists together
for i in range(len(Xindex)+len(XindexHave)):
	try:
		if XindexHave[0] > Xindex[0]:
			XindexAll.append(Xindex.pop(0))
			y_predAll.append(y_pred.pop(0))
		else:
			XindexAll.append(XindexHave.pop(0))
			y_predAll.append(y_predHave.pop(0))

	# perhaps one list is empty
	except IndexError:
		if not Xindex:
			XindexAll.append(XindexHave.pop(0))
			y_predAll.append(y_predHave.pop(0))
		if not XindexHave:
			XindexAll.append(Xindex.pop(0))
			y_predAll.append(y_pred.pop(0))



vec3 = np.array([0,0])
vec2 = np.array([1/2, np.sqrt(3/4)])
vec1 = np.array([1,0])


for i in range(len(XindexAll)):
	XindexAll[i] = A[i]*vec1 + B[i]*vec2 + C[i]*vec3

x = [array[0] for array in XindexAll]
y = [array[1] for array in XindexAll]

cm = plt.cm.get_cmap('RdYlBu')
sc = plt.scatter(x, y, c=y_predAll, cmap=cm)
plt.colorbar(sc)
plt.show()


XindexAll = map(str, XindexAll)
y_predAll = map(str, y_predAll)


with open('interpolatedPhasestheta0.005.txt', 'w') as f:
	f.write(','.join(y_predAll)+'\n')
	f.write('\n')
	f.write(','.join(XindexAll))






