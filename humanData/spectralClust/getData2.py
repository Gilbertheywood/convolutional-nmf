import pandas, os
from collections import defaultdict
import collections
import numpy as np
import sklearn.cluster as cluster
import cPickle as pickle


import sys
sys.path.insert(0, '../')
import readCsv

"""
TODO 

the script takes like 5 minutes to run for all data
make sure I know all one-off errors

shouldn't I make sure null phase is connected?
"""


	
class reader:


	def __init__(self):

		#self.myDict = defaultdict(list)
		self.subm = readCsv.cleanSubmissionData()
		self.tasks = readCsv.getTaskData()
		self.pairHash = defaultdict(list)
		self.singleHash = defaultdict(list)

	
	def readAllSubm(self):

		for submission in self.subm.iterrows():
			
			slices, peakAssign = self.readEntry(submission)
			self.addEntryHash(slices, peakAssign)

	def addEntryHash(self, slices, peakAssign):
		'''
		adds one entry to the hashtable
		'''

		# iterate over every pair of peaks - note that we get every relationship twice
		for row in range(len(peakAssign)):
			for peak in range(len(peakAssign[row])):

				if peakAssign[row][peak] !=0: 
					self.singleHash[(slices[row], peak)].append(1) # for each nonzero element

				for row2 in range(len(peakAssign)):
					for peak2 in range(len(peakAssign[row2])):
						if peakAssign[row][peak] == peakAssign[row2][peak2] and peakAssign[row][peak] !=0 and (row,peak) != (row2, peak2):
							self.pairHash[((slices[row], peak), (slices[row2], peak2))].append(1) # for nonzero nonequal elements in same phase
						else:
							self.pairHash[((slices[row], peak), (slices[row2], peak2))].append(0) # for nonequal, or at least one is nullphase
	
	
	def readEntry(self, submission):
		'''
		reads one submission and returns list of slices and phases
		submission is pandas series, as in data.iterrow
		'''

		task = submission[1][2]
		taskData = self.tasks[self.tasks['id'] == task]
		trowID = int(taskData['targetRowNum'])
		slices = map(int, list(taskData['slice'])[0].split(','))

		peakAssign = submission[1][5][2:-2].split('],[')
		peakAssign = [myString.split(',') for myString in peakAssign]
		trow = map(int, submission[1][7][1:-1].split(','))

		# NOTE one-off error
		peakAssign[trowID-1] = trow

		for row in range(len(peakAssign)):
			for peak in range(len(peakAssign[row])):
				if peakAssign[row][peak] == 'null': 
					peakAssign[row][peak] =0
				else:
					peakAssign[row][peak] = trow[int(peakAssign[row][peak])-1] # NOTE one-off error


		

		return slices, peakAssign


	def makeMat(self, limit=-1):
		'''
		gives np matrix and the keys to read it by
		'''

		keys = [item for item in list(self.singleHash) if sum(self.singleHash[item])>limit]
		mat = np.zeros([len(keys),len(keys)])

		for i in range(len(keys)):
			for j in range(len(keys)):
				mat[i][j] = sum(self.pairHash[(keys[i], keys[j])])/float(0.001 + len(self.pairHash[(keys[i], keys[j])])) # add eps to avoid divide by zero

		return keys, mat


	def giveCluster(self, mat, components=13):
		'''
		gives the Spectral Clustering of the assigned matrix
		'''
		myModel = cluster.SpectralClustering(n_clusters=components, affinity='precomputed')
		return myModel.fit_predict(mat)


	
myReader = reader()
myReader.readAllSubm()
keys,mat= myReader.makeMat()
cluster = myReader.giveCluster(mat)

pickle.dump( keys, open( "keys.p", "wb" ) )
pickle.dump( cluster, open( "cluster.p", "wb" ) )


#keys = pickle.load( open( "keys.p", "rb" ) )
#cluster = pickle.load(open('cluster.p', 'rb'))

#print collections.Counter(cluster)
#phase = [keys[i][0] for i in range(len(keys)) if cluster[i]==7]
#print phase
