# coding=utf-8

import cPickle as pickle
import numpy as np
import random


# TODO - important
# make sure I can read artificial data
# not sure how to generate turkerinput with many components - relabeling symmetry?
# There is also relabeling symmetry for their input
# What happens when I input more clusters then what the turkers have inputted?



# TODO - later
# todo refactor genTurk - use same code as in taskTensor
# todo - what happens if vote breaks down in genTurk - put in random place?
# must make sure that rows are comatible in more functions
# should state taskID explicitly in every function
# have what in diagonal for adjecency matrix?
# make sure indexing is correct in pickler and otherwise
# need to make sure sizeweight works
# give alternative weightnings in taskTensor
# otherway to connect matrix?
# need to redo sizeweight
# write tests for all functions, find a suitable database to look at, 
# make illustrations - just output same csv-file and give to Richs code?
# switch to pandas for data collection?
# have test that makes sure submissions are compatible?
# decide where to have numpy, and where to have lists - decide when to use 0/1 for null phase


def input2Mat(input):
	# gives matrix of phase alignment, adds offset for phaseID
	align = input[0]
	tRow = input[1] 
	myVec = []

	for row in align:
		for peak in row:
			if peak != None: 
				myVec.append(tRow[peak])
			else:
				myVec.append(0)

	phaseVec = np.array(myVec + tRow, dtype = int)+1 # adds one offset, needed for vector multiplication
	return np.outer(phaseVec,phaseVec)


def equalWeight(input):
	# for three phases
	mat = input2Mat(input)
	for i in [1,2,3,4]:
		mat[mat == i**2] = 1
	mat[mat != 1] =0
	return mat



def genTaskdata(task, prob=1):

	usableData = pickle.load( open( "save.p", "rb" ))
	taskData = [] # list over submissions with correct taskID
	for subm in range(len(usableData)):
		if usableData[subm][2] == task and random.random()<prob:
			taskData.append(usableData[subm][:2]) # removes taskID

	print taskData[0]


def taskTensor(taskData, weight='equal', epsilon=0.001):


	myTensor = np.zeros((len(taskData),) + input2Mat(taskData[0]).shape) #creates 3tensor

	for subm in range(len(taskData)):
		if weight == 'equal':
			myTensor[subm,:] = equalWeight(taskData[subm])

	# aggregates and pads 3tensor to a 2tensor usable for spectral clustering
	return np.sum(myTensor, axis=0) + epsilon*np.ones(myTensor[0].shape)




genTaskdata(2430)












