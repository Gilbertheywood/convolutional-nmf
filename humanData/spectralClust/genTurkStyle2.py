import cPickle as pickle
import pandas
import ast
import copy
import collections
import operator


import sys
sys.path.insert(0, '../')
import readCsv



'''
Script for illustrating the clustering along specific tasks
'''


'''
make sure invariant under nullphase permutation
how should allocation be made? just connect to trow if same phase?
needs reverse peakassignments...
'''


def reading(task=2430):
	'''
	creates all the variables needed downstream
	'''

	# reads the slicenames
	taskData = readCsv.getTaskData()
	taskData = taskData[taskData['id'] == task]
	slices = list(ast.literal_eval(list(taskData['slice'])[0]))
	trowName =  int(taskData['imgPeakSelected'])

	# find all submissions, used for voting later
	submData = readCsv.cleanSubmissionData()
	allSubms = submData[submData['taskId'] == task]
	allSubms = allSubms.values.tolist()
	allSubms = [ast.literal_eval(someSubm[5].replace('null','None')) for someSubm in allSubms]

	# finds the peaks
	submData = readCsv.cleanSubmissionData()
	submData = submData[submData['taskId'] == task].loc[submData[submData['taskId'] == task].index.tolist()[0]]
	peakAssign = ast.literal_eval(submData['peakassignments'].replace('null','None'))
	trow = ast.literal_eval(submData['targetrowlinestatus'])

	# create a list of the indices of the rows
	indices = copy.deepcopy(peakAssign)
	for row in range(len(indices)):
		for peak in range(len(indices[row])):
			indices[row][peak] = (slices[row], peak)

	return slices, trowName, peakAssign, trow, indices, allSubms


def assignPhase(task=2430):
	'''
	assign phases according to clustering
	'''
	slices, trowName, peakAssign, trow, indices, allSubms = reading(task=task)
	keys = pickle.load( open( "keys.p", "rb" ) )
	cluster = pickle.load(open('cluster.p', 'rb'))
	print 'assume nullphase as {}'.format(max(collections.Counter(cluster).iteritems(), key=operator.itemgetter(1))[0])
	nullphase = max(collections.Counter(cluster).iteritems(), key=operator.itemgetter(1))[0]

	phases = copy.deepcopy(indices)

	# some peaks are never picked, and not part of the keys
	for row in range(len(indices)):
		for peak in range(len(indices[row])):
			try:
				phase = cluster[keys.index(indices[row][peak])]
			except ValueError:
				phase = nullphase

			phases[row][peak] = phase

	return phases, nullphase


def genTurk(task=2430):
	'''
	generates readable output
	'''

	slices, trowName, peakAssign, trow, indices, allSubms = reading(task=task)
	phases, nullphase = assignPhase(task=task)
	trowIndex = slices.index(trowName)

	print phases

	for row in range(len(peakAssign)):

		if row == trowIndex:
			for peak in range(len(peakAssign[row])):
				peakAssign[row][peak] = trowIndex+1 #note one-off correction
				trow[peak] = phases[row][peak]
		else:
			for peak in range(len(peakAssign[row])):
				if phases[row][peak] == nullphase:
					peakAssign[row][peak] = None
				else:
					# connect to most common in trow
					votesTRow = [subm[row][peak] for subm in allSubms if subm[row][peak] != None]
					# possibly none assigned them, thenassume nullphase 
					if not votesTRow:
						peakAssign[row][peak] = None
						print 'cant find majority vote'
					else:
						commonVote = max(set(votesTRow), key = votesTRow.count)
						if phases[trowIndex][commonVote] == phases[row][peak]: #TODO have one-off error here?
							peakAssign[row][peak] = commonVote
							print 'made connection with phase {}'.format(phases[row][peak])
						else:
							print 'common vote doesnt share phase'
							peakAssign[row][peak] = None



	newPeakAssign = str(peakAssign).replace(" ","").replace("None","null")
	newTrow = str(trow).replace(str(nullphase),'0')

	# redo the phase labeling
	phasesList = set(phases[trowIndex])
	phasesList = list(phasesList - set([nullphase]))
	for phase in range(len(phasesList)):
		if phasesList[phase] != nullphase:
			newTrow = newTrow.replace(str(phasesList[phase]),str(phase+1))

	return newPeakAssign, newTrow, trowIndex, slices

#2451 seems good
print genTurk(task=2451)