import pandas, os
from collections import defaultdict
import collections
import numpy as np
import readCsv
import itertools


"""
TODO 
make sure I know all one-off errors
"""

	
class phaseAggregator:


	def __init__(self):
		"""
		creates the tasks, submissions and make list of set of phases
		"""
		self.subm = readCsv.cleanSubmissionData()
		self.tasks = readCsv.getTaskData()
		self.phaseSets = []
		self.readAllSubm()
		#print self.phaseSets

	
	def readAllSubm(self):
		"""
		read through all submissions and add them to phaseSets, remove empty sets
		"""

		for submission in self.subm.iterrows():
			
			slices, peakAssign = self.readEntry(submission)
			self.addPhaseSet(slices, peakAssign)

		emptyIndices = []
		for phase in range(len(self.phaseSets)):
			if not self.phaseSets[phase]: emptyIndices.append(phase)

		for index in sorted(emptyIndices, reverse=True):
			del self.phaseSets[index]



	def addPhaseSet(self, slices, peakAssign):
		'''
		adds one entry to the sets of phases
		'''

		numSets = max(set(itertools.chain.from_iterable(peakAssign)))
		phaseSet = [set([]) for i in range(numSets)]


		for row in range(len(peakAssign)):
			for peak in range(len(peakAssign[row])):
				if peakAssign[row][peak] !=0: 
					phaseSet[peakAssign[row][peak]-1].add((slices[row], peak))

		for phase in phaseSet:
			self.phaseSets.append(phase)
		


	def readEntry(self, submission):
		'''
		reads one submission and returns list of slices and phases
		submission is pandas series, as in data.iterrow
		'''

		task = submission[1][2]
		taskData = self.tasks[self.tasks['id'] == task]
		trowID = int(taskData['targetRowNum'])
		slices = map(int, list(taskData['slice'])[0].split(','))

		peakAssign = submission[1][5][2:-2].split('],[')
		peakAssign = [myString.split(',') for myString in peakAssign]
		trow = map(int, submission[1][7][1:-1].split(','))

		# NOTE one-off error
		peakAssign[trowID-1] = trow

		for row in range(len(peakAssign)):
			for peak in range(len(peakAssign[row])):
				if peakAssign[row][peak] == 'null': 
					peakAssign[row][peak] =0
				else:
					peakAssign[row][peak] = trow[int(peakAssign[row][peak])-1] # NOTE one-off error

		return slices, peakAssign


	
#myAgg = phaseAggregator()


