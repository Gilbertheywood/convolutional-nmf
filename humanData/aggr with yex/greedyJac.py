from __future__ import division
from phaseAggregator import phaseAggregator
import readCsv


"""
todo - introduce randomness
make more efficient, don't recalculate
"""


class GreedyJacardi(phaseAggregator):
	"""
	class that greedily merges turker phases based on jacardi index
	keeps track of nmbr of merged turkers, and values
	"""

	def __init__(self):
		"""
		creates the tasks, submissions and make list of set of phases
		"""
		self.subm = readCsv.cleanSubmissionData()
		self.tasks = readCsv.getTaskData()
		self.phaseSets = []
		self.readAllSubm()
		self.turkCounts = [1 for phase in self.phaseSets]


	def mergeRepeat(self, critVal=0.8):

		bestVal = 1

		while bestVal > critVal:
			bestVal = self.mergeBest(critVal=critVal)

		return



	def mergeBest(self, critVal=0.5):
		"""
		finds the best merger, does the merge if it has value less then some value, returns best value found
		"""

		bestVal=0
		bestIndex = (0,0)

		# finds the best merger and its value
		for set1 in range(len(self.phaseSets)):
			for set2 in range(len(self.phaseSets)):
				if set1 != set2:
					currVal = len(self.phaseSets[set1] & self.phaseSets[set2])/len(self.phaseSets[set1] | self.phaseSets[set2])

					if currVal > bestVal:
						bestVal = currVal
						bestIndex = (set1, set2)

		# if better then critical value, merge phases
		if bestVal > critVal:
			print "do merge"
			newPhase = self.phaseSets[bestIndex[0]] & self.phaseSets[bestIndex[1]]
			newCount = self.turkCounts[bestIndex[0]] + self.turkCounts[bestIndex[1]]

			for i in sorted(bestIndex, reverse=True): #delete items in reverse
				del self.phaseSets[i] 
				del self.turkCounts[i]

			self.phaseSets.append(newPhase)
			self.turkCounts.append(newCount)

		print bestVal, max(self.turkCounts)
		return bestVal




myJac = GreedyJacardi()
myJac.mergeRepeat()