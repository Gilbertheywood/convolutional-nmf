import pandas, os
import collections



'''
some submissions from us
rl454 is ronan, cpg5 is carla, rbv2 is bruce, uploaders and answerkey are by ronan, rab38 is rich, yexiang yx247
last summer class - around Juli 2015
this summer class - late Juli 2016
doesn't seem to be any misc data - just 81 summer student submissions
'''


'''
the eng participation file contains the netids of student participating this summer
'''


def cleanSubmissionData(art=False):
	'''
	removes submissions from ronan, rich etc, and just looks at the Rh-Ta system
	'''

	data = pandas.read_csv('hit_submissions.csv')
	names = ['rl454', 'solutionuploader', 'partialsolutionuploader','answerkey', 'njb225', 'rab38', 'rbv2', 'yx247']

	for name in names:
		data = data[data['workerId'] != name]

	if art:
		data = data[data['taskId']<1500]
	else: 
		data = data[data['taskId']>1500]

	return data


def getTaskData():
	return pandas.read_csv('uditurk_tasks.csv')


#data = cleanSubmissionData()
#print len(list(data['workerId'].unique()))
#print collections.Counter(data['taskId'])


