all: main.cpp AgileCombiFD.cpp
	g++ -c AgileCombiFD.cpp -I/home/fs01/jb2467/usr/include -L/home/fs01/jb2467/usr/lib -O2 -larmadillo -lgflags -w
	g++ -c main.cpp -I/home/fs01/jb2467/usr/include -L/home/fs01/jb2467/usr/lib -O2 -larmadillo -lgflags -w
	g++ AgileCombiFD.o main.o -o conv_nmf -I/home/fs01/jb2467/usr/include -L/home/fs01/jb2467/usr/lib -O2 -larmadillo -lgflags -w

clean:
	rm -f *.o conv_nmf
