#include <iostream>
#include "AgileCombiFD.h"
#include <gflags/gflags.h>

using namespace google;
//using namespace google;

DEFINE_string(inst, "", "Input instance file");
DEFINE_string(sol, "output.txt", "The output file name of the solution");
DEFINE_int64(k, 8, "k: The number of phases");
DEFINE_int64(m, 10, "m: The number of possible different shifts");
DEFINE_int64(time, 100000000, "time: The maximum time(seconds) spent to train the model that you could accept.");
DEFINE_double(c, 0.00008, "Related to termination criterion: In one iteration, if (old_cost-new_cost)<c*old_cost, then the loop terminates.");
DEFINE_int64(seed, -1, "seed: random seed for the random number generator.(The default value -1 means time(0))");

static bool validate_inst(const char* inst, const string& value) {
    if (value != "") {
        return true;
    }
    cout << "Input filename could not be empty!" << endl;
    return false;
}

static const bool pass_inst = RegisterFlagValidator(&FLAGS_inst, &validate_inst);

using namespace std;

Instance read_inst(char* filename) {
    freopen(filename, "r", stdin);
    Instance inst;
    char c;
    string buf;
    int sampleNo = 0;
    //printf("1\n");
    while ((c = getchar())!=EOF) {
        //printf("ccccc: %c\n", c);
        if (c == 'N') {
            char cc = getchar();
            if (cc != '=') {
                getline(cin, buf);
                continue;
            }
            cin >> inst.N;
            getchar();
            //printf("inst.N!!!:  %d\n", inst.N);
        } else if (c == 'Q') {
            char cc = getchar();
            if (cc != '=') {
                getline(cin, buf);
                continue;
            }
            double qvalue;
            while (scanf("%lf", &qvalue) != -1) {
                inst.qvalues.push_back(qvalue);
                char cc = getchar();
                if (cc == 10) break;
                if (cc == 13) {getchar();break;}
            }
            inst.L = inst.qvalues.size();
            //printf("[[[[%d, %d]]]]\n", inst.L, inst.N);
            inst.XrdMat = zeros<mat>(inst.L, inst.N);
        } else if (c == 'I') {
            //printf("4\n");
            //printf("sampleNo: %d\n", sampleNo);
            //int t;
            //scanf("%d", &t);getchar();

            char cc = getchar();
            bool isXrd = true;
            while (cc != '=') {
                //cout << cc << endl;
                if (isdigit(cc)) cc = getchar();
                else {
                    isXrd = false;
                    break;
                }
            }
            if (!isXrd) {
                getline(cin, buf);
                //cout << buf << endl;
                continue;
            }

            double xrd;
            int cnt = 0;
            //printf("5\n");
            while (scanf("%lf", &xrd) != -1) {
                //printf("XrdMat size: %d, %d\n", inst.XrdMat.n_cols, inst.XrdMat.n_rows);
                inst.XrdMat(cnt, sampleNo) = xrd;
                char cc = getchar();
                if (cc == 10) break;
                if (cc == 13) {getchar();break;}
                cnt++;
            }
            sampleNo++;
        } else {
            getline(cin, buf);
            //cout << buf << endl;
        }
    //printf("**\n");
    }
    cout << "L: " << inst.L << endl;
    cout << "N: " << inst.N << endl;
    cout << "size of qvalues: " << inst.qvalues.size() << endl;
    cout << "XrdMat: (" << inst.XrdMat.n_rows << " " << inst.XrdMat.n_cols << ")\n" << endl;
    return inst;
}

Instance generate_inst(AgileCombiFDParameters &param) {
    Instance inst;
    inst.L = 3;
    inst.N = 6;
    param.K = 3;
    param.M = 1;

    inst.qvalues.push_back(0.1);inst.qvalues.push_back(0.2);inst.qvalues.push_back(0.3);
    inst.XrdMat = zeros<mat>(inst.L, inst.N);

    inst.XrdMat(0,0) = 0.1; inst.XrdMat(1, 0) = 0.2; inst.XrdMat(2, 0) = 0.3;
    inst.XrdMat(0,1) = 0.1; inst.XrdMat(1, 1) = 0.1; inst.XrdMat(2, 1) = 0.1;
    inst.XrdMat(0,2) = 0.3; inst.XrdMat(1, 2) = 0.1; inst.XrdMat(2, 2) = 0.3;
    inst.XrdMat(0,3) = 0.2; inst.XrdMat(1, 3) = 0.3; inst.XrdMat(2, 3) = 0.4;
    inst.XrdMat(0,4) = 0.4; inst.XrdMat(1, 4) = 0.2; inst.XrdMat(2, 4) = 0.4;
    inst.XrdMat(0,5) = 0.4; inst.XrdMat(1, 5) = 0.3; inst.XrdMat(2, 5) = 0.6;

    /*for (int i = 0; i < inst.L; i++) {
        inst.qvalues.push_back((i+1)/10.0);
    }

    inst.XrdMat = zeros<mat>(inst.L, inst.N);
    param.initW = randu<mat>(inst.L, param.K);
    param.initH = randu<mat>(param.K, inst.N)+randu<mat>(param.K, inst.N)/1000.0;

    inst.XrdMat = param.initW * param.initH;*/

    return inst;
}

void generateWH(char* filename, int L, int N, int K, AgileCombiFDParameters &param) {
    freopen(filename, "r", stdin);
    char c;
    string buf;
    int phaseNo = 0, index = 0, sampleNo = 0;

    mat iW = zeros<mat>(L, K);
    mat iH = zeros<mat>(K, N);
    //printf("%d\n", N);
    while ((c = getchar())!=EOF) {
        if (c == 'B') {
            int t;
            scanf("%d", &t);getchar();
            double v;
            index = 0;
            while (scanf("%lf", &v) != -1) {
                iW(index, phaseNo) = v;
                index++;
                char cc = getchar();
                if (cc == 10) break;
            }
            phaseNo++;
        } else if (c == 'C') {
            //printf("*\n");
            double v;
            int t;
            scanf("%d", &t);getchar();
            index = 0;
            //printf("%d\n", sampleNo);
            while (scanf("%lf", &v) != -1) {
                //printf("index: %d\n", index);
                iH(index, sampleNo) = v;
                char cc = getchar();
                if (cc == 10) break;
                index++;
            }
            //printf("**\n");
            sampleNo++;
        } else {
            getline(cin, buf);
        }
    }
    param.initH = iH;
    param.initW = iW;
}

int main(int argc, char **argv)
{
    SetUsageMessage("Welcome to PHASES Detector!");
    ParseCommandLineFlags(&argc, &argv, true);
    //freopen("output.txt", "w", stdout);
    //cout << "Hello world!" << endl;
    AgileCombiFDParameters param(FLAGS_m, FLAGS_k);
    
    if (FLAGS_seed == -1) srand(time(0));
    else srand(FLAGS_seed);

    //Instance inst = generate_inst(param);
    /*char filename[100];
    printf("Please enter the input instance file name: ");
    cin >> filename;
    printf("Please enter the predicted number of phases: ");
    cin >> param.K;*/

    //Instance inst = read_inst((char*)"syth_inst/Ac-Ag-Au_inst.txt");

    Instance inst = read_inst((char*)(FLAGS_inst.c_str()));
    //generateWH((char*)"Ac-Ag-Au_sol.txt", inst.L, inst.N, param.K, param);

    AgileCombiFD conv_nmf(param);

    conv_nmf.Solve(inst, FLAGS_c, FLAGS_time, (char*)FLAGS_sol.c_str());
    return 0;
}
